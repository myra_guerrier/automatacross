console.log("========================================\nImports:");

var express = require('express'),
	mongo = require('mongodb').MongoClient,
	missionBuilder = require("./server/missionBuilder.js"),
	mapBuilder = require("./server/mapBuilder.js");

// connect to the mongoDB database

var dbMethods = {};

mongo.connect(process.env.MONGOHQ_URL || "mongodb://localhost/automata-cross", function(error, db)
{
	console.log("\nConnecting to the database...");

	if(error)
	{
		console.log(" > Database connection error:");
		console.log(error);
		return;
	}

	console.log(" > Connection successful!");

	db.collection("users", function(error, collection)
	{
		if(error)
		{
			console.log(" > Failed to connect to collection 'users':");
			console.log(error);
			return;
		}

		console.log(" > Got 'users' collection, generating utility functions...");
	})
});

var app = express.createServer();

// static file serving
console.log("\nStarting up static file server...");
app.use(express.static(__dirname + "/client"));

// testing app.get stuff
app.get("/test", function(req, res)
{
	res.send("Test packet!");
});

app.get("/new-mission", function(req, res)
{
	res.send(missionBuilder.build());
});

app.get("/new-map", function(req, res)
{
	res.send(mapBuilder.build());
});

app.listen(process.env.PORT || 3000);