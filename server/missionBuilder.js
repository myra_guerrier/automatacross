// A framework for generating Missions, returned as JSON strings.  Most of the bulk processing work is done on the client, but we set up all base parameters here.

console.log("Loaded missionBuilder.js");

exports.build = function()
{
	var m = {
		type : [
			"Survival",
			"Target Elimination",
			"Object Recovery",
			"Location Defense"
			][Math.floor(Math.random() * 4)],
		objectives : [
			"Test objective A",
			"Test objective B",
			],
		seed : Math.floor(Math.random() * 1000000),	// random integer between 0 and 1 million
		faction : [
			"Inner Systems Military",
			"Space Pirates",
			"Warlords",
			"Trader's guild"
			][Math.floor(Math.random() * 4)],				// random selection from list
		location : [
			"Ouroboros System",
			"Compass D.S.I.",
			"Obscura System",
			"Farspan System",
			"Ignis System"
			][Math.floor(Math.random() * 5)],
		reward : ((Math.floor(Math.random() * 10) * 50) + 250) // generate in increments of 50, with a minimum of 250
	};

	return JSON.stringify(m);
}