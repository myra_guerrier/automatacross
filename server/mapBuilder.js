// Generates a map, as a JSON file, to send to the client

console.log("Loaded mapBuilder.js");

exports.build = function(setting, seed)
{
	var map = [[]],
		collision = [],
		aiSpawns,
		dir = {			// markers to OR with each map tile, setting the first 2 bits to be a direction for the tile to be rotated
			n : 0,
			w : 1,
			s : 1 << 1,
			e : (1 << 1 | 1)
		},
		t = {			// tile IDs
			nul : 0,
			flt : 1 << 2,
			wll : 2 << 2,
			crn : 3 << 2,
			hll : 4 << 2,
			ent : 5 << 2,
			cap : 6 << 2
		},
		c = {			// tile rotations
			nul : [{
				x : 0,
				y : 0,
				width : 256,
				height : 256
			}],
			flt : [],
			wll : [{
				x : 0,
				y : 0,
				width : 64,
				height : 256
			}],
			crn : [{
				x : 0,
				y : 0,
				width : 64,
				height : 256
			},
			{
				x : 0,
				y : 0,
				width : 256,
				height : 64
			}],
			hll : [{
				x : 0,
				y : 0,
				width : 64,
				height : 256
			},
			{
				x : 192,
				y : 0,
				width : 64,
				height : 256
			}],
			ent : [{
				x : 0,
				y : 0,
				width : 64,
				height : 64
			},
			{
				x : 192,
				y : 0,
				width : 64,
				height : 64
			}],
			cap : [{
				x : 0,
				y : 0,
				width : 64,
				height : 64
			}]
		},

		// recursive copy function
		copy = function(obj){
			var result = (obj.length) ? [] : {};

			for(p in obj)
			{
				result[p] = (typeof obj[p] === "object") ? copy(obj[p]) : obj[p]; 
			}

			return result;
		};

	// hard-code a map for testing
	map = [
		[t.crn|dir.n, t.wll|dir.e, t.crn|dir.e, t.nul|dir.n, t.crn|dir.n, t.wll|dir.e, t.wll|dir.e, t.crn|dir.e],
		[t.wll|dir.n, t.flt|dir.n, t.wll|dir.s, t.nul|dir.n, t.wll|dir.n, t.cap|dir.s, t.cap|dir.w, t.wll|dir.s],
		[t.crn|dir.w, t.cap|dir.w, t.cap|dir.e, t.wll|dir.e, t.cap|dir.n, t.cap|dir.e, t.cap|dir.n, t.wll|dir.s],
		[t.nul|dir.n, t.crn|dir.w, t.wll|dir.w, t.wll|dir.w, t.wll|dir.w, t.wll|dir.w, t.wll|dir.w, t.crn|dir.s]
	];

	for(var i = 0, lim = map.length; i < lim; i++)
	{
		collision.push([]);

		for(var j = 0, l = map[i].length; j < l; j++)
		{
			var r = 0,
				p1 = {x:0,y:0},
				p2 = {x:0,y:0};

			for(k in t)
			{
				if((map[i][j] & ~ 3) === t[k])
				{
					//console.log("c[k]:");
					//console.log(c[k]);

					r = copy(c[k]);

					//console.log("r:");
					//console.log(r);
				}
			}

			//console.log(r);

			for(var k = 0, kLim = r.length; k < kLim; k++)
			{

				switch(map[i][j] & 3)
				{
					// account for the tile not being rotated at all, this gets converted back to base form by the code later on
					case 0:
						p1 = {
							x : r[k].x,
							y : r[k].y
						};
						p2 = {
							x : p1.x + r[k].width,
							y : p1.y + r[k].height
						}
						break;

					// tile facing west
					case 1:
						p1 = {
							x : 256 - r[k].y,
							y : 256 - r[k].x
						};
						p2 = {
							x : 256 - (r[k].y + r[k].height),
							y : 256 - (r[k].x + r[k].width)
						};
						break;

					// tile facing south
					case 2:
						/*p1 = {
							x : -(128 - r[k].x) + 128,
							y : -(128 - r[k].y) + 128
						};
						p2 = {
							x : -(128 - (r[k].x + r[k].width)) + 128,
							y : -(128 - (r[k].y + r[k].height)) + 128
						};*/

						/*p1 = {
							x : -r[k].x + 256,
							y : -r[k].y + 256
						};
						p2 = {
							x : -r[k].x + r[k].width + 256,
							y : -(r[k].y + r[k].height) + 256
						}*/

						p1 = {
							x : r[k].x + 256 - r[k].width,
							y : r[k].y + 256 - r[k].height
						};

						p2 = {
							x : r[k].x + 256,
							y : r[k].y + 256
						};

						break;

					// tile facing east
					case 3:
						/*p1 = {
							x : -(128 - r[k].y) + 128,
							y : -(128 - r[k].x) + 128
						};
						p2 = {
							x : -(128 - (r[k].y + r[k].height)) + 128,
							y : -(128 - (r[k].x + r[k].width)) + 128
						};*/

						/*p1 = {
							x : 256 - r[k].y,
							y : r[k].x
						};

						p2 = {
							x : 256 - r[k].y + r[k].height,
							y : r[k].x + r[k].width
						}*/

						p1 = {
							x : 0,
							y : 0
						};
						p2 = {
							x : 0,
							y : 0
						};

						break;

					default:
						console.log("Map direction index error!");
						console.log(map[i][j] & 3);
						break;
				}

				// convert from two points to one point plus width and height
				if(p1.x < p2.x)
				{
					r[k].x = p1.x;
					r[k].width = p2.x - p1.x;
				}
				else
				{
					r[k].x - p2.x;
					r[k].width = p1.x - p2.x;
				}

				if(p1.y < p2.y)
				{
					r[k].y = p1.y;
					r[k].height = p2.y - p1.y;
				}
				else
				{
					r[k].y = p2.y;
					r[k].height = p1.y - p2.y;
				}
			}

			//console.log("Transformed at ");
			//console.log(map[i][j] & 3);
			//console.log(r);

			collision[i].push(r);
		}
	}

	// add in hard-coded AI spawn locations
	aiSpawns = [
		{
			type : "drone",
			x : 450, y : 600
		},
		{
			type : "drone",
			x : 550, y : 550
		},
		{
			type : "testAI",
			x : 650, y: 700
		},
		{
			type : "testAI",
			x : 750, y: 775
		},
		{
			type : "testAI",
			x : 1000, y: 900
		},
		{
			type : "drone",
			x : 1200, y: 475
		},
		{
			type : "drone",
			x : 1350, y: 400
		},
		{
			type : "drone",
			x : 1300, y: 550
		},
		{
			type : "boss",
			x : 1600, y: 350
		}
	];

	return JSON.stringify({
		map : map,
		collision : collision,
		aiSpawns : aiSpawns,
		tileset : "metal"
	});
}