// provides app-level functionality for dealing with time

var app = app || {};

app.time = 
(function()
{
	var startTime = getSystemTime();

	// return the current system time in seconds
	function getSystemTime()
	{
		return new Date().getTime() / 1000.0;
	}
	
	// gets the current time
	function getTime()
	{
		return getSystemTime() - startTime;
	}
	
	// resets the current time
	function resetTime()
	{
		startTime = getSystemTime();
	}
	
	return{
		getTime: getTime,
		resetTime: resetTime
	}
})();