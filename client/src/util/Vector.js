var app = app || {};

app.vector = {};

app.vector.add = function( vec1, vec2 )
{
	return { x: vec1.x + vec2.x,
			 y: vec1.y + vec2.y };
}

app.vector.subtract = function( vec1, vec2 )
{
	return { x: vec1.x - vec2.x,
			 y: vec1.y - vec2.y };
}

app.vector.scalarMul = function( vec, s )
{
	return { x: vec.x * s,
			 y: vec.y * s };
}

app.vector.length = function( vec )
{
	return Math.sqrt( (vec.x * vec.x) + (vec.y * vec.y) );
}

app.vector.normalize = function( vec )
{
	var l = app.vector.length(vec);
	return (l > 0) ? { x: vec.x / l, y: vec.y / l } : {x: 0, y: 0};
}

app.vector.distanceSquared = function(vec1, vec2)
{
	var a = (vec1.x - vec2.x) * (vec1.x - vec2.x),
		b = (vec1.y - vec2.y) * (vec1.y - vec2.y);

	return a + b;
}

app.vector.distance = function( vec1, vec2 )
{
	var a = (vec1.x - vec2.x) * (vec1.x - vec2.x),
		b = (vec1.y - vec2.y) * (vec1.y - vec2.y);
		
	return Math.sqrt(a + b);
}

app.vector.dot = function( vec1, vec2 )
{
	return ((vec1.x * vec2.x) + (vec1.y * vec2.y));
}

// Rotates the vector a quarter turn counterclockwise
app.vector.perp = function( vec )
{
	return { x: -vec.y,
			 y: vec.x };
}

app.vector.vectorTo = function(start, end)
{
	var result = {
		x : end.x - start.x,
		y : end.y - start.y
	};

	return app.vector.normalize(result);
}

app.vector.compare = function(a, b, dist)
{
	return (app.vector.distanceSquared(a, b) < (dist * dist || 5 * 5));
}