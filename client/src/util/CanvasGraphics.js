// CanvasGraphics.js -- common methods for Canvas manipulation

var app = app || {};

app.graphics =
(function()
{
	
	var ctx;
	
	// Sets the context for the graphics module
	this.setContext = function(ctx)
	{
		this.ctx = ctx;
	}
	
	// traces a circle path
	this.circle = function(x,y,r)
	{
		this.ctx.beginPath();
		this.ctx.arc(x, y, r, 0, 2 * Math.PI);
	}
	
	// generates a circular stroke
	this.strokeCircle = function(x,y,r)
	{
		this.circle(x,y,r);
		this.ctx.stroke();
	}
	
	// generates a filled circle
	this.fillCircle = function(x,y,r)
	{
		this.circle(x,y,r);
		this.ctx.fill();
	}
	
	// trace a rectangular path, with curved corners
	this.roundedRect = function(x,y,width,height,r)
	{
		this.ctx.beginPath();
		
		// first, if the Radius is less than one pixel, just return a regular rectangle
		if(r < 1)
		{
			this.ctx.rect(x,y,width,height);
			return;
		}
		
		// start tracing clockwise, from the top left corner
		this.ctx.moveTo(x+r, y);
		this.ctx.lineTo((x+width)-r, y);
		// make the top right arc
		this.ctx.arc((x+width)-r, y+r, r, 1.5*Math.PI, 0);
		// right side
		this.ctx.lineTo((x+width), (y+height)-r);
		// botton right arc
		this.ctx.arc((x+width)-r, (y+height)-r, r, 0, 0.5*Math.PI);
		// bottom side
		this.ctx.lineTo(x+r, (y+height));
		// bottom left arc
		this.ctx.arc((x+r), (y+height-r), r, 0.5*Math.PI, Math.PI);
		// left side
		this.ctx.lineTo(x, y+r);
		// top left arc
		this.ctx.arc(x+r, y+r, r, Math.PI, 1.5*Math.PI);
	}
	
	// creates a stroked rounded rectancle
	this.strokedRoundRect = function(x,y,width,height,r)
	{
		this.roundedRect(x,y,width,height,r);
		this.ctx.stroke();
	}
	
	// create a filled rounded rectangle
	this.filledRoundRect = function(x,y,width,height,r)
	{
		this.roundedRect(x,y,width,height,r);
		this.ctx.fill();
	}
	
	return{
		setContext: setContext,
		circle: circle,
		strokeCircle: strokeCircle,
		fillCircle: fillCircle,
		roundedRect: roundedRect,
		strokedRoundRect: strokedRoundRect,
		filledRoundRect: filledRoundRect
	}
})();