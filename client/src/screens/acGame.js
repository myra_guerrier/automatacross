var app = app || {};
app.screens = app.screens || {};

app.screens.acGame = (function()
{
	var firstRun = true;

	function init()
	{
		// testing code -- create and deploy a player object
		var player = new app.game.mech();
		player.speed = 175;
		player.pos = {x : 250, y : 250}
		player.target = player.pos;
		player.type = "player";
		player.components['input'] = app.components.inputHandler.factory({}, player);
		player.components['shields'] = app.components.shieldGenerator.factory({}, player);
		player.components['weapons'].params.delay = 75;
		player.maxHP = 250;
		player.hp = 250;

		ping = app.game.ping;

		app.game.init();
		app.game.addObj(player);
		app.game.addObj(ping);
		app.game.controller.init(player, ping);
		app.game.cam.init();
		app.game.cam.follow(player);
	}

	function onEnter()
	{
		if( firstRun )
		{
			firstRun = false;
			init();
		}
	}

	function onExit()
	{
		//app.game.state.pause();
	}

	return {
		onEnter : onEnter,
		onExit : onExit
	}

})();