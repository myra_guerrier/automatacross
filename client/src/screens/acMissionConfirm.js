var app = app || {};
app.screens = app.screens || {};

app.screens.acMissionConfirm = (function()
{
	var firstRun = true;

	function init()
	{
		$("#acMconfirm").click(function()
		{
			app.activateScreen("acGame");
		});
	}

	function updateDisplay(mission)
	{
		$("#acMtype").text(mission.type);
		$("#acMfaction").text(mission.faction);
		$("#acMlocation").text(mission.location);

		$("#acMobjectives").empty();
		for(var i = 0, num = mission.objectives.length; i < num; i++)
		{
			$("#acMobjectives").append("<li>" + mission.objectives[i] + "</li>");
		}

		$("#acMreward").text(mission.reward.toString() + " credits");
	}

	function onEnter()
	{
		if(firstRun)
		{
			firstRun = false;
			init();
		}
	}

	function onExit()
	{

	}

	return {
		updateDisplay : updateDisplay,
		onEnter : onEnter,
		onExit : onExit
	}

})();