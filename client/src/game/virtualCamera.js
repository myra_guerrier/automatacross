var app = app || {};
app.game = app.game || {};

app.game.cam = (function()
{
	var pos = {
		x : 0,
		y : 0
	},
		target = 0,
		params = {
			speed : 300
		};

	function init(start)
	{
		if(start)
		{
			pos = start;
		}

		// x and y position getters and setters
		app.game.cam.__defineGetter__("x", function()
		{
			return pos.x - (document.width / 2);
		});

		app.game.cam.__defineSetter__("x", function(val)
		{
			updatePos("x", val);
		});

		app.game.cam.__defineGetter__("y", function()
		{
			return pos.y - (document.height / 2);
		});

		app.game.cam.__defineSetter__("y", function(val)
		{
			updatePos("y", val);
		});

		app.game.cam.__defineGetter__("pos", function()
		{
			return pos;
		});
	}

	function updatePos(axis, amount)
	{
		// TODO: clamping of camera positional values
		switch(axis)
		{
			case "x":
				pos.x = amount;
				break;

			case "y":
				pos.y = amount;
				break;

			default:
				console.log(" > Warning!  Called app.game.cam.updatePos() with an invalid axis!");
				break;
		}
	}

	function update(dt)
	{
		// object tracking code
		if(target !== 0)
		{
			// are we following by id or by reference?
			var t = (target.pos) ? target : app.game.getObj(target);

			// check if we are adjacent to the target already or not
			if(app.vector.compare(pos, t.pos, 100))
			{
				pos = t.pos;
			}
			// if not, move us to be
			else
			{
				pos = app.vector.add(
						app.vector.scalarMul(
							app.vector.vectorTo(pos, t.pos),
							params.speed * dt
						),
						pos
					);
			}
		}
	}

	function follow(id)
	{
		target = id;
	}

	function unfollow()
	{
		target = 0;
	}

	return {
		init : init,
		updatePos : updatePos,
		follow : follow,
		unfollow : unfollow,
		update : update
	}

})();