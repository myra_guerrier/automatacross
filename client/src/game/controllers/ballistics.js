// handles bullets, rockets, etc, in a centralized and data-driven manner

var app = app || {};
app.controllers = app.controllers || {};

app.controllers.ballistics = (function()
{

	shots = [];

	function init()
	{
		this.tracer = $("#sTracer")[0];
		this.flare = $("#sFlare")[0];
		this.missile = $("#sMissile")[0];

		console.log("Initialized Ballistics controller!");
	}

	function boom( b )
	{
		if(b.mode === "missile")
		{
			var iter = 0,
				ls = app.game.simList,
				limit = ls.length;

			for( iter; iter < limit; iter++ )
			{
				if( ls[iter].id && ls[iter].type !== b.faction )
				{
					if( app.vector.compare( ls[iter].pos, b.pos, 75 ) )
					{
						ls[iter].damage( b.damage );
					}
				}
			}

			app.controllers.particleSystem.newEmitter(
			{
				count : 3,
				spawnRate : 0,
				lifetime : 0.15,
				systemLifetime : 1,
				initVelocity : 25,
				velocityJitter : 15,
				acceleration : -50,
				initPos : b.pos,
				spawnRadius : 1,
				color : "rgba(255,64,0,1)",
				randomSpin : true,
				angularVelocity : 2,
				angularAcceleration : -1,
				initScale : 1,
				endScale : 3
			},
			"sFire");

			app.controllers.particleSystem.newEmitter(
			{
				count : 15,
				spawnRate : 0,
				lifetime : 0.25,
				systemLifetime : 1,
				initVelocity : 300,
				velocityJitter : 200,
				acceleration : -700,
				initPos : b.pos,
				spawnRadius : 1,
				color : "rgba(255,64,0,1)",
				initScale : 1.5,
				endScale : 0.25
			},
			"sGlow");

			app.controllers.particleSystem.newEmitter(
			{
				count : 10,
				spawnRate : 0,
				lifetime : 0.75,
				systemLifetime : 1,
				initVelocity : 75,
				velocityJitter : 10,
				acceleration : -125,
				initPos : b.pos,
				spawnRadius : 15,
				color : "rgba(255,64,0,1)",
				randomSpin : true,
				angularVelocity : 2,
				angularAcceleration : -1,
				initScale : 1,
				endScale : 1.75
			},
			"sBoom");
		}
	}

	function registerShot( bulletData, origin, target, mode )
	{
		var b = {
			pos : origin,
			target : target,
			vec : app.vector.vectorTo(origin, target),
			vel : bulletData['velocity'] || 1000,
			sprite : bulletData['sprite'] || "bullet",
			mode : mode || "linear",
			damage : bulletData["damage"],
			faction : bulletData["faction"]
		};

		// move the bullet up so that it does not collide with the mech shooting it
		b.pos = app.vector.add(
				b.pos,
				app.vector.scalarMul(
					app.vector.vectorTo(origin, target),
					48
				)
			);

		var r = Math.atan(
				(b.target.y - b.pos.y) /
				(b.target.x - b.pos.x)
			);

		r = (b.target.x - b.pos.x > 0) ? r : r + Math.PI;

		app.game.ctx.save();
		app.game.ctx.translate( b.pos.x - app.game.cam.x, b.pos.y - app.game.cam.y );
		app.game.ctx.rotate( r + Math.PI/2 );
		app.game.ctx.drawImage(this.flare, - 16, - 16);
		app.game.ctx.restore();

		shots.push(b);
	}

	function update( dt, ctx )
	{
		var i = 0;

		for(i; i<shots.length; i++)
		{
			var b = shots[i],
				r = Math.atan(
						(b.target.y - b.pos.y) /
						(b.target.x - b.pos.x)
					);

			r = (b.target.x - b.pos.x > 0) ? r : r + Math.PI;

			b.pos = app.vector.add(
					b.pos,
					app.vector.scalarMul( b.vec, (dt * b.vel) )
				);


			var c = app.game.checkCollision(b.pos);

			if(c)
			{
				// don't allow friendly fire
				if(c.type !== b.faction)
				{
					if( b.mode === "missile" )
					{
						console.log("Called boom()");
						boom( b );
					}
					else
					{
						c.damage(b.damage);
					}

					shots.splice(i,1);
					i--;
					continue;
				}
			}

			if(b.pos.x - app.game.cam.x < 0 ||
				b.pos.y - app.game.cam.y < 0 ||
				b.pos.x - app.game.cam.x > app.game.params.boardSize.width ||
				b.pos.y - app.game.cam.y > app.game.params.boardSize.height)
			{
				shots.splice(i,1);
				i--;
				continue;
			}

			if(b.mode == "targeted" || b.mode === "missile")
			{
				if(app.vector.compare(b.pos, b.target, 20))
				{
					if( b.mode === "missile" )
						boom( b );

					shots.splice(i,1);
					i--;
					continue;
				}
			}

			if( app.game.map.checkCollision(b.pos) )
			{
				shots.splice(i,1);
				i--;
				continue;
			}

			ctx.save();
			//ctx.fillStyle = "rgba(255,255,255,1)";
			//ctx.fillRect( b.pos.x - 2 - app.game.cam.x, b.pos.y -2 - app.game.cam.y, 4, 4 );

			ctx.save();
			ctx.translate(b.pos.x - app.game.cam.x, b.pos.y - app.game.cam.y);
			ctx.rotate( r + (Math.PI/2) );
			if(b.mode === "missile")
			{
				ctx.drawImage(this.missile, -8, -8);
			}
			else
			{
				ctx.drawImage(this.tracer, -8, -8);
			}
			
			ctx.restore();

			ctx.restore();
		}
	}

	return {
		init : init,
		registerShot : registerShot,
		update : update
	}

})();