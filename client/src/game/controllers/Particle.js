// a base data structure for a particle

var app = app || {};
app.game = app.game || {};

app.game.particle = function(params)
{
	var p = {};
	
	p.pos = params.pos;
	p.vel = params.vel;
	p.vec = params.vec;
	p.spin = params.spin;
	p.spinVel = params.spinVel;
	p.life = -1 * params.lifeJitter * Math.random() || 0;
	
	return p;
}