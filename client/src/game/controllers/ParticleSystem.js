// Contains functionality nessecary to create and manage particle emitters

var app = app || {};
app.controllers = app.controllers || {};

app.controllers.particleSystem = (function()
{

	var emitterList = [];

	function init()
	{
		console.log("Initialized Particle controller!");
	};

	// instantiates a named particle system we have already defined somewhere
	function newByName(name, position)
	{
		/*var callback = function(result)
		{
			// for each particle layer in the json object, create a new sprite
			for(i in result)
			{
				newEmitter(
					result,
					result.sprite
				)
			}
		}
	
		app.ajaxLoader.load("json", ("player/Resource/ParticleSystems/" + name + ".json"), callback);*/
		
		var p = app.game.particles[name],
			i = 0,
			container = {
				systems : [],
				update : function(dt, ctx)
				{
					var i = 0;
					for(i; i < this.systems.length; i++)
					{
						this.systems[i].update(dt, ctx);
					}
				}
			};
		
		for(i; i<p.length; i++)
		{
			container.systems.push(newEmitter(
					p[i],
					p[i].particle
				));
		}
		
		console.log(container);
		
		return container;
	}

	// creates a new particle emitter and adds it to the simulation
	// params -- an object containing simultion parameters
	// sprite -- either a reference to an offscreen canvas, a primative shape as a string ("circle" is the only supported option for now), or another emitter
	function newEmitter(params, sprite)
	{
		var e = new app.game.particleEmitter;
		
		// base simulation parameters
		e.count = params.count || 10;						// max particles alive at any time
		e.spawnRate = params.spawnRate || 0;				// how often to spawn new particles
		e.lifetime = params.lifetime || 5;					// how long to keep the particle alive, in seconds
		e.lifeJitter = params.lifeJitter || 0;				// amount to vary lifetime by
		e.systemLifetime = params.systemLifetime || -999;	// how long to keep the emitter alive
		
		e.initVelocity = params.initVelocity || 1;			// float
		e.velocityJitter = params.velocityJitter || 0;		// amount to vary starting velocity by
		e.initPos = params.initPos || {x : 100, y : 100};	// start location
		e.spawnRadius = params.spawnRadius || 5;			// radius around start location in which to spawn particles
		e.acceleration = params.acceleration || 0;			// particle acceleration (set to a negative for a dampning effect)
		e.gravity = params.gravity || {x : 0, y : 0};		// vector, X and Y component
		e.angularVelocity = params.angularVelocity || 0; 	// float, radians per second
		e.angularAcceleration = params.angularAcceleration || 0; // float, radians per second to accelerate or shed
		e.initAlpha = params.initAlpha || 1;				// float, opacity of the particle to start at
		e.initScale = params.initScale || 1;				// starting scale
		e.endScale = params.endScale || 1;					// ending scale

		e.color = params.color || "rgba(255,255,255,0.5)";
		e.size = params.size || 32;
		
		e.randomSpin = params.randomSpin || false;			// Randomize our starting angle?
		e.spawnVectorRange = params.spawnVectorRange || { start : 0, end : Math.PI * 2 };	// an angled cone to spawn particles within, sets initial vector
		
		// sprite stuff
		if(typeof sprite === "string")
		{
			// create an offscreen buffer canvas, and draw the requested shape to it
			var buffer = $("<canvas />")[0];
			
			buffer.width = 32;
			buffer.height = 32;
			
			var ctx = buffer.getContext("2d");
		
			switch(sprite)
			{
				case "circle":
					ctx.save();
					ctx.fillStyle = e.color;
				
					app.graphics.setContext(ctx);
					app.graphics.fillCircle(16,16,16);
					break;
					
				default:
					ctx.drawImage($("#"+sprite)[0], 0,0);
					break;
			}
			
			e.particleSprite = buffer;
		}
		else
		{
			console.log("Error!  Sprite is not a string!");
			console.log(sprite);
		}
		
		e.init();

		emitterList.push(e);

		// return the newly created particle system
		return e;
	}
	
	function update( dt, ctx )
	{
		var i = 0,
			lim = emitterList.length;

		for(i; i<lim; i++)
		{
			emitterList[i].update( dt, ctx );
		}

		i = 0;

		// check for system elements to delete
		for(i; i < emitterList.length; i++)
		{
			if( emitterList[i].delete )
			{
				emitterList.splice(i, 1);
				i--;
			}
		}
	}

	return {
		init : init,
		newEmitter : newEmitter,
		newByName : newByName,
		update : update
	}

})();