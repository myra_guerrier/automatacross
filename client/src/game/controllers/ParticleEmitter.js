// A particle emitter object, which contains functionality for simulating and updating all particles within its scope
// Particle emitters extend RealTimeEntities

var app = app || {};
app.game = app.game;

app.game.particleEmitter = app.game.characterEntity.extend({

	/*particleList : [],
	spawnCounter : 0,*/
	
	// creates a particle based on our parameters, and adds it to our internal particle listing
	generateParticle : function()
	{
		var theta = (this.spawnVectorRange.end - this.spawnVectorRange.start) * Math.random(),
		p = {
			pos : {
				x : this.initPos.x + this.spawnRadius * ((Math.random() * 2) - 1),
				y : this.initPos.y + this.spawnRadius * ((Math.random() * 2) - 1)
			},
			vel : this.initVelocity + ( this.velocityJitter * ((Math.random() * 2) -1) ) || 0,
			vec : {
				x : Math.cos( theta ),
				y : Math.sin( theta )
			},
			spin : this.randomSpin?(Math.PI * 2 * Math.random()):0,
			spinVel : this.angularVelocity,
			lifeJitter : this.lifeJitter || 0
		};
		
		this.particleList.push(new app.game.particle(p));
	},
	
	init : function()
	{
		this.particleList = [];
		this.spawnCounter = 0;

		if( this.spawnRate === 0 )
		{
			var diff = this.count - this.particleList.length;

			if(diff > 0)
			{
				for(var c = 0; c < diff; c++)
				{
					this.generateParticle();
				}
			}
		}
	},

	// extends the base update function
	update : function(dt, ctx)
	{
		// quick hack to reposition this where we want it
		//this.position = this.initPos;
	
		//this._super(dt, ctx);	// run the base update function, for now (we'll remove this later, since it draws something we don't need except for debugging)
		
		this.spawnCounter += dt;
		
		// check if we should add a new particle to the simulation
		if(this.particleList.length < this.count && this.systemLifetime > 0  && this.spawnRate !== 0)
		{
			if(this.spawnCounter > this.spawnRate)
			{
				this.spawnCounter = 0;
				this.generateParticle();
			}
		}

		/*if(this.spawnRate === 0)
		{

			var diff = this.count - this.particleList.length;

			if(diff > 0)
			{
				for(var c = 0; c < diff; c++)
				{
					this.generateParticle();
				}
			}
		}*/
		
		// update all particles in the simulation, taking into account that particles might themselves be emitters
		var i = 0;
		for(i; i < this.particleList.length; i++)
		{
			var p = this.particleList[i];
		
			if(p.life > this.lifetime)
			{
				this.particleList.splice(i,1);
				i--;
				continue;
			}
		
			// first, check if p has an update function
			// if it does, we can assume p is an emitter and run it's update step
			if(p.update)
			{
				p.update(dt, ctx);
				continue;
			}
		
			// update the parameters of the particle object
			p.pos = app.vector.add(
				p.pos,
				app.vector.scalarMul(
					app.vector.add(
						app.vector.scalarMul(p.vec, p.vel),
						this.gravity
					),
					dt
				)
			);
			
			p.vel = p.vel + this.acceleration * dt;
			
			p.spin = p.spin + ((this.angularVelocity + this.angularAcceleration) * dt);
			
			ctx.save();
			ctx.translate(p.pos.x - app.game.cam.x, p.pos.y - app.game.cam.y);
			ctx.rotate(p.spin);

			var s = ((p.life / this.lifetime) * (this.endScale - this.initScale)) + this.initScale;

			p.life = p.life + dt;
			
			ctx.globalAlpha = Math.min(Math.max(Math.sin((p.life/this.lifetime) * Math.PI), 0), 1);
			
			// now, draw the particle that we just updated
			ctx.drawImage(this.particleSprite, 0,0, this.size,this.size, -1 * (this.size * s)/2, -1 * (this.size * s)/2, this.size * s, this.size * s);
			
			ctx.restore();
		}

		// check if we should stop existing
		if(this.systemLifetime !== -999)
		{
			this.systemLifetime -= dt;

			/*if(this.systemLifetime + this.lifetime <= 0)
			{
				this.delete = true;
				return;
			}*/

			if( this.particleList.length === 0 )
			{
				this.delete = true;
				return;
			}
		}
	}
	
});