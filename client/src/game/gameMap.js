var app = app || {};
app.game = app.game || {};

app.game.map = (function(){

	var map = [[0]],
		col = [[0]],
		tileData = 0;

	// returns the root map object
	function getMap()
	{
		return map;
	}

	// returns the tile sprite
	function getTiles()
	{
		return tileData;
	}

	// returns the collision data object
	function getCollision()
	{
		return col;
	}

	// checks for collisions at the point p, within the specified bounds
	// if no bounds are set, defaults to 'within camera field of view'
	// p is set in world-space coordinates, mouse clicks should be converted before being passed in
	function checkCollision(p, bounds)
	{
		// double-checking that we were passed in a bounding box -- if not, set it to the camera pos
		bounds = bounds || app.game.cam.pos;
		bounds.x = bounds.x || 0;
		bounds.y = bounds.y || 0;
		bounds.width = bounds.width || $("body").width();
		bounds.height = bounds.height || $("body").height();

		//p.x = p.x + app.game.cam.x;
		//p.y = p.y + app.game.cam.y;

		// quick check -- determine if the requested point is actually within our specified bounding box
		/*if(p.x < bounds.x || p.x > bounds.x + bounds.width || p.y < bounds.y || p.y > bounds.y + bounds.height)
		{
			return true;
		}*/

		// first, build a list of collision objects which are within our bounds
		var toCheck = [];

		for(var i = col.length; i--;)
		{
			for(var j = col[i].length; j--;)
			{
				/*// vertical positioning check
				if((i * 256) + 256 > bounds.y && i * 256 < bounds.y + bounds.height)
				{
					// horizontal positioning check
					if((j * 256) + 256 > bounds.x && j * 256 < bounds.x + bounds.width)
					{*/
						//toCheck.push(col[i][j]);

						for(var l = 0; l < col[i][j].length; l++)
						{
							rect = col[i][j][l];

							// horizontal check
							if(p.x > rect.x + (j * 256) && p.x < rect.x + rect.width + (j * 256))
							{
								// vertical check
								if(p.y > rect.y + (i * 256) && p.y < rect.y + rect.height + (i * 256))
								{
									return true;
								}
							}
						}
					/*}
				}*/
			}
		}

		return false;

		// log stuff
		//console.log("Filtered list: ");
		//console.log(toCheck);

		// second, iterate through the list we just generated and do bounds-checking
		/*for(var k = 0, lim = toCheck.length; k < lim; k++)
		{
			var lim2 = toCheck[k].length,
				rect;

			for(var l = 0; l < lim2; l++)
			{
				rect = toCheck[k][l];

				// horizontal check
				if(p.x > rect.x + (k * 256) && p.x < rect.x + rect.width + (k * 256))
				{
					// vertical check
					if(p.y > rect.y + (l * 256) && p.y < rect.y + rect.height + (l * 256))
					{
						console.log("Hit object: x = " + (rect.x + (k * 256)) + ", y =" + (rect.y + (l * 256)) + ", w = " + rect.width + ", h =" + rect.height);
						return true;
					}
				}
			}
		}*/

		// if we didn't just get a collision, return false, since there was none to get
		//return false;

	}

	function loadMap(mapdata, collision, tiles, callback)
	{
		var tileset = $("<img/>");
		tileset.attr("src", ("tiles/" + tiles + ".png"));

		// use the imagesloaded plugin to check when the map tileset is loaded, and run our callback when it is
		tileset.imagesLoaded(function()
		{
			console.log(collision);
			map = mapdata;
			tileData = tileset[0];
			col = collision;
			callback();
		});

	}

	return {
		loadMap : loadMap,
		getMap : getMap,
		getTiles : getTiles,
		getCollision : getCollision,
		checkCollision : checkCollision
	}

})();