var app = app || {};
app.game = app.game || {};

app.game.controller = (function()
{

	var playerObj = 0,
		mousePos = {x:0, y:0},
		initialOrientation = true,
		initialAngles = { alpha : 0, beta : 0, gamma : 0 };

	function init(player, ping)
	{
		playerObj = player;

		// mouse position handler
		$("body").mousemove(function(event)
		{
			mousePos.x = event.pageX;
			mousePos.y = event.pageY;

			playerObj.heading = {
				x : mousePos.x + app.game.cam.x,
				y : mousePos.y + app.game.cam.y};

			playerObj.components.input.mousePos( playerObj.heading );
		});

		// left-click handler
		$("#gameBoard").click(function(event)
		{
			event.preventDefault();

			// for now, default to shooting, add handlers for other events later
			var target = { 
				x : event.pageX + app.game.cam.x,
				y : event.pageY + app.game.cam.y
			}

			playerObj.components.weapons.shoot(playerObj.pos, target);

			return false;
		});

		$("#gameBoard").mousedown(function(event)
		{
			if(event.which === 1)
				playerObj.components.input.mouseDown();
		});

		$("#gameBoard").mouseup(function(event)
		{
			if(event.which === 1)
				playerObj.components.input.mouseUp();
		});

		$("#gameBoard").bind("touchmove", function(event)
		{
			event.preventDefault();
			var t = event.originalEvent.touches[0];

			mousePos.x = t.pageX;
			mousePos.y = t.pageY;

			playerObj.heading = {
				x : mousePos.x + app.game.cam.x,
				y : mousePos.y + app.game.cam.y};

			playerObj.components.input.mousePos( playerObj.heading );
		});

		$("#gameBoard").bind("touchstart", function(event)
		{
			event.preventDefault();
			var t = event.originalEvent.touches[0];

			// if we tapped the center of the screen, zero accelerometer offsets
			if( app.vector.compare( 
					{ x : t.pageX, y : t.pageY },
					{ x : app.game.params.boardSize.width / 2, y : app.game.params.boardSize.height / 2 },
					60
			 ) )
			{
				initialOrientation = true;
				return;
			}

			ping.at(
				{x : t.pageX + app.game.cam.x,
				 y : t.pageY + app.game.cam.y}
				);

			mousePos.x = t.pageX;
			mousePos.y = t.pageY;

			playerObj.heading = {
				x : mousePos.x + app.game.cam.x,
				y : mousePos.y + app.game.cam.y};

			playerObj.components.input.mousePos( playerObj.heading );
			playerObj.components.input.mouseDown();
		});

		$("#gameBoard").bind("touchend", function(event)
		{
			event.preventDefault();

			playerObj.components.input.mouseUp();
		});

		// right-click handler
		$("#gameBoard").bind("contextmenu", function(event)
		{
			event.preventDefault();

			playerObj.controlMode = 0;

			ping.at(
				{x : event.pageX + app.game.cam.x,
				 y : event.pageY + app.game.cam.y}
				);

			var vec = {
				x : event.pageX - (app.game.params.boardSize.width/2),
				y : event.pageY - (app.game.params.boardSize.height/2)
			}

			//collision disabled until we've debugged it a bit more fully
			if(! app.game.map.checkCollision(
				{x : event.pageX + app.game.cam.x,
				 y : event.pageY + app.game.cam.y},
				 app.game.cam.pos ) )
			{
				playerObj.moveTo(vec);
				console.log(vec);
			}
			else
			{
				console.log("Hit collision area!");
			}

			return false;
		});

		if(window.DeviceOrientationEvent == undefined)
		{
			alert("No deviceorientation support");
		}
		else
		{
			// Jquery didn't seem to like deviceorientation, so we're using the built-in event listener
			window.addEventListener("deviceorientation", function(e)
				{
					if(initialOrientation)
					{
						initialOrientation = false;
						initialAngles.alpha = e.alpha;
						initialAngles.beta = e.beta;
						initialAngles.gamma = e.gamma;

						return;
					}

					playerObj.controlMode = 1;
					var bearing = { x: 0, y: 0 };

					/*if(e.beta > 45)
					{
						//playerObj.shift({x:0, y:5});

						bearing.y = (e.beta - 45) / 20;
					}

					if(e.beta < 35)
					{
						//playerObj.shift({x:0, y:-5});

						bearing.y = -((35 - e.beta) / 20);
					}

					if(e.gamma > 5)
					{
						//playerObj.shift({x:5, y:0});

						bearing.x = (e.gamma - 5) / 20;
					}

					if(e.gamma < -5)
					{
						//playerObj.shift({x:-5, y:0});

						bearing.x = (e.gamma + 5) / 20;
					}*/

					if(e.beta > initialAngles.beta + 6)
					{
						bearing.y = (e.beta - initialAngles.beta - 6) / 20;
					}

					if(e.beta < initialAngles.beta - 6)
					{
						bearing.y = ( e.beta - initialAngles.beta + 6) / 20;
					}

					if(e.gamma > initialAngles.gamma + 4)
					{
						bearing.x = (e.gamma - initialAngles.gamma - 4) / 20;
					}

					if(e.gamma < initialAngles.gamma - 4)
					{
						bearing.x = (e.gamma - initialAngles.gamma + 4) / 20;
					}

					if(bearing.x > 1) { bearing.x = 1 };
					if(bearing.y > 1) { bearing.y = 1 };

					playerObj.bearing = bearing;

				}, false);
			}

		/*if(window.DeviceMotionEvent == undefined)
		{
			alert("No DeviceMotion support");
		}
		else
		{
			alert("DeviceMotion appears to be supported");

			try
			{
				window.addEventListener("devicemotion", function(e)
				{
					alert("DeviceMotion");
					alert(JSON.stringify(e));
				}, false);
			}
			catch(E)
			{
				alert(E);
			}
		}*/
	}

	return {
		init : init
	}

})();