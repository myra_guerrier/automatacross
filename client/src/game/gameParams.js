var app = app || {};
app.game = app.game || {};

app.game.params = {

	fixedUpdateTime : 0.1,
	boardSize : {width : 100, height : 100},
	collisionDebug : true
}