var app = app || {};
app.game = app.game || {};

app.game.ctx = 0;
app.game.simList = [];
app.game.lastFixedUpdate = 0;

app.game.init = function()
{
	var canvas = $("#gameBoard")[0],
		body = $("body"),
		params = app.game.params;

	// disable text selection
	body.attr('unselectable', 'on')
		.css('user-select', 'none')
		.on('selectstart', false);

	params.boardSize.width = body.width();
	params.boardSize.height = body.height();

	canvas.width = params.boardSize.width;
	canvas.height = params.boardSize.height;

	app.game.ctx = canvas.getContext("2d");

	app.game.ctx.save();
	app.game.ctx.font = "bold 16px Arial";
	app.game.ctx.fillText("Loading...", params.boardSize.width/2,params.boardSize.height/2);
	app.game.ctx.restore();

	console.log("Initialized the game!");

	// load up a map
	$.ajax({
		url : app.config.root_url + "/new-map"
	}).done(function(result)
	{
		var m = JSON.parse(result);

		for(var i = 0; i < m.aiSpawns.length; i++)
		{
			var mech = app.game.mechFactory( m.aiSpawns[i].type );
			mech.pos = { x : m.aiSpawns[i].x, y : m.aiSpawns[i].y };
			mech.target = mech.pos;
		}

		app.game.map.loadMap(m.map, m.collision, m.tileset, function()
		{
			app.game.launch();
		});
	});

	// start up all of our controllers
	for(i in app.controllers)
	{
		app.controllers[i].init();
	}

	//requestAnimationFrame(app.game.update);
}

app.game.launch = function()
{
	// start the game
	requestAnimationFrame(app.game.update);
}

// get the list position of an object by id
app.game.getObjIndex = function(id)
{
	var l = app.game.simList;
	for(var i = 0, lim = l.length; i < lim; i++)
	{
		if(l[i].id === id)
		{
			return i;
		}
	}

	return undefined;
}

// get an object in the simulation by id
app.game.getObj = function(id)
{
	return app.game.simList[app.game.getObjIndex(id)];
}

// add an object to the simulation
app.game.addObj = function(obj)
{
	app.game.simList.push(obj);
}

// remove an object from the simulation
app.game.removeObj = function(obj)
{
	var index = app.game.getObjIndex(obj.id);
	app.game.simList.splice(index, 1);
}

// checks the positions of all the entities in the simulation, and returns the first which matches the given parameter P
app.game.checkCollision = function( p, r )
{
	var i = 0,
		l = app.game.simList
		lim = l.length;

	for(i; i < lim; i++)
	{
		if(l[i].id)
		{
			if( app.vector.compare( l[i].pos, p, (r || 20) ) )
			{
				return l[i];
			}
		}
	}

	return false;
}

app.game.query = function( q )
{
	var i = 0,
		l = app.game.simList,
		lim = l.length;

	for(i; i < lim; i++)
	{
		if(l[i].query)
			l[i].query( q );
	}
}

// update the simulation state
app.game.update = function()
{
	var dt = app.game.state.getDt(),
		ctx = app.game.ctx,
		l = app.game.simList,
		params = app.game.params,
		map = app.game.map.getMap(),
		tiles = app.game.map.getTiles(),
		collision = app.game.map.getCollision();

	// clear out the screen
	//ctx.fillRect(0,0,params.boardSize.width, params.boardSize.height);

	// variable background image for specular tiles
	ctx.drawImage($("#sky")[0], 0, 0, params.boardSize.width, params.boardSize.height);

	// update the camera position
	app.game.cam.update(dt);

	//ctx.drawImage(tiles, 0,0);

	// draw the map
	for(var i = 0, ylim = map.length; i < ylim; i++)
	{
		for(var j = 0, xlim = map[i].length; j < xlim; j++)
		{
			// first, do a bounds check on the map tiles
			if((j * 256) + 256 < app.game.cam.x || j * 256 > app.game.cam.x + app.game.params.width)
			{
				continue;
			}

			var t = map[i][j],
				tID = t >>> 2,		// extract the bits after the first two, holding the ID of the tile
				tDir = t & 3;		// grab the last two bits, the direction that the tile is oriented

			tDir = -(Math.PI*0.5*tDir);

			ctx.save();

			ctx.translate((256*j) + 128 - app.game.cam.x, (256*i) + 128 - app.game.cam.y);
			ctx.rotate(tDir);
			ctx.drawImage(tiles, 256*tID,0, 256,256, -128,-128, 256,256 );

			ctx.restore();

			// collision debug drawing
			if(app.config.debug_draw === true)
			{
				var c = collision[i][j];

				ctx.save();
				ctx.fillStyle = "rgba(0,0,255,0.5)";

				for(var k = 0, kLim = c.length; k < kLim; k++)
				{
					ctx.fillRect(
						(256 * j) - app.game.cam.x + c[k].x,
						(256 * i) - app.game.cam.y + c[k].y,
						c[k].width,
						c[k].height
						);
				}
				ctx.restore();
			}
		}
	}

	// fill the edges of the screen with black
	if(app.game.cam.y <= 0)
		ctx.fillRect(0,0, params.boardSize.width, -1 * app.game.cam.y + 1);

	if(app.game.cam.x <= 0)
		ctx.fillRect(0,0, -1 * app.game.cam.x + 1, params.boardSize.height);

	if(app.game.cam.y + params.boardSize.height >= map.length * 256)
		ctx.fillRect(0, -1 * (app.game.cam.y) + (map.length * 256) - 1, params.boardSize.width, params.boardSize.height);

	if(app.game.cam.x + params.boardSize.width >= map[0].length * 256)
		ctx.fillRect(-1 * (app.game.cam.x) + (map[0].length * 256) - 1, 0, params.boardSize.width, params.boardSize.height);

	//if(app.game.cam.y + params.boardSize.height >= )

	app.game.lastFixedUpdate -= dt;

	requestAnimationFrame(app.game.update);

	if(app.game.state.isPaused()){return;}

	// Run controller updates
	for(i in app.controllers)
	{
		app.controllers[i].update( dt, ctx );
	}

	// Run update
	for(var i = 0; i < l.length; i++)
	{
		l[i].update(dt, ctx);
	}

	// run a second pass through and remove any objects which should be deleted
	for(i = 0; i < l.length; i++)
	{
		if(l[i].delete === true)
		{
			l.splice(i, 1);
			if(l.length > 0)
			{
				i--;
			}
			else
			{
				break;
			}
			continue;
		}
	}

	// run fixedupdate
	if(app.game.lastFixedUpdate <= 0)
	{

		for(var j = 0; j < l.length; j++)
		{
			if(l[j].fixedUpdate)
			{
				l[j].fixedUpdate((params.fixedUpdateTime - app.game.lastFixedUpdate), ctx);
			}

			// this functionality is done in the root Mech update code
			/*if(l[j].components)
			{
				for(c in l[j].components)
				{
					if(c.update)
					{
						c.update( dt. ctx );
					}
				}
			}*/
		}

		app.game.lastFixedUpdate = params.fixedUpdateTime;
	}
}