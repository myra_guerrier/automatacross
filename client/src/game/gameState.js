var app = app || {};
app.game = app.game || {};

app.game.state = (function()
{
	var paused = false,
		lastUpdateTime = 0,
		dtCap = 1,
		idCounter = 0;

	function isPaused() { return paused; }

	function pause() { paused = true; }

	function unpause() { pause = false; }

	function getDt()
	{
		var ct = new Date().getTime() / 1000,
			dt = ct - lastUpdateTime;

		lastUpdateTime = ct;
		dt = (dt < dtCap) ? dt : dtCap;	// cap the deltaTime, so that we don't have unpredictable behavior if the browser hangs

		return dt;
	}

	function getUniqueID()
	{
		return ++idCounter;
	}

	return {
		isPaused : isPaused,
		pause : pause,
		unpause : unpause,
		getDt : getDt,
		getUniqueID : getUniqueID
	}

})();