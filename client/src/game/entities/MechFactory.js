// constructs mechs with the specified input parameters

var app = app || {};
app.game = app.game || {};

app.game.mechFactory = function( params, callback )
{
	switch( (typeof params) )
	{
		// ----------------------------------------------------------------------------------------------------------------------------------
		// create a mech based on the named config file
		case "string":

			switch(params)
			{
				// ----------------------------------------------------------------------------------------------------------------------------------
				case "testAI":
					var m = new app.game.mech();

					m.components['ai'] = app.components.basicAIPackage.factory( {}, m );
					m.pos = {x : 400, y : 400};
					m.target = m.pos;

					m.maxHP = 50;
					m.hp = 50;

					app.game.addObj( m );

					return m;

					break;

				// ----------------------------------------------------------------------------------------------------------------------------------
				case "drone":
					var m = new app.game.mech();

					m.speed = 75;
					m.maxSpeed = 75;
					m.sprite = $("#sSmallMech")[0];

					delete m.components['weapons'];

					m.components['selfDestruct'] = app.components.selfDestruct.factory( {}, m );
					m.components['ai'] = app.components.basicAIPackage.factory( { type : "charge" }, m );

					m.pos = {x : 400, y : 400};
					m.target = m.pos;

					app.game.addObj( m );

					m.maxHP = 15;
					m.hp = 15;

					return m;

					break;

				// ----------------------------------------------------------------------------------------------------------------------------------
				case "boss":
					var m = new app.game.mech();

					m.sprite = $("#sBossMech")[0];

					m.components['missiles'] = app.components.weaponSystem.factory( { delay : 750, velocity : 350, damage : 25, type : "missile" }, m );
					m.components['ai'] = app.components.basicAIPackage.factory( { type : "volley", volleyDelay : 3, volleyLength : 3, range : 350 }, m );
					m.components['shields'] = app.components.shieldGenerator.factory( { rechargeDelay : 6, rechargeRate : 3, capacity : 15 }, m );

					m.pos = { x : 400, y : 400 };
					m.target = m.pos;	

					app.game.addObj( m );

					return m;

				default:
					console.warn("Called mechFactory with unknown mech id: " + params);
			}

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		// apply parameters from the params object
		case "object":

			var m = new app.game.mech();

			if( params.components )
			{
				for(c in params.components)
				{
					m.components[params.components[c]['name']] = app.components[params.components[c]['class']].factory(params.components[c]['params'], m);
				}
			}

			if( params.variables )
			{
				for(v in params.variables)
				{
					m[v] = params.variables[c];
				}
			}

			app.game.addObj( m );

			return m;

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		default:

			console.warn("Called mechFactory without valid parameters!");

			return;
	}
}