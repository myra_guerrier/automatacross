// A turn-based entity that knows how to be a character, not normally to be used in the simulation, since it is neither an NPC, nor a player character
// Characters are implemented to have internal finite state machines, for handling state changes, and animation events.

var app = app || {};
app.game = app.game || {};

app.game.characterEntity = app.game.entity.extend({

	// animation functionality
	anim : {
		animationSets : {		// a set of names tied to frame ranges, played through in order
			neutral : [0]
		},
		animation : "neutral",	// current animation
		_frameIndex : 0,		// index to the current frame of the animation
		_frameCounter : 0,		// timer
		framerate : 0.5,		// in frames per second
	},
	
	sprite : "entitySprite",

	addAnim : function(name, set)
	{
		this.anim.animationSets[name] = set;
	},
	
	setAnim : function(name)
	{
		this.anim.animation = name;
	},
	
	// functionality for an internal finite state machine
	_state : 0,
	_stateList : [],
	
	addState : function(name, onEnter, onTic, onExit)	// Pushes a state to the finite state machine's internal list
	{
		var state = {
			name : name,
			onEnter : onEnter,
			onExit : onExit,
			onTic : onTic
		};
		
		this._stateList.push( state );
	},
	setState : function( index )	// Changes our internal state TODO: Error handling for invalid state indexes
	{
		var i = 0;
	
		// check if we have a state yet, if not, there is no 'onExit' function to call
		if( this._state )
		{
			// run onExit function
			for(i; i < this._stateList.length; i++)
			{
				if(this._stateList[i].name === this._state)
				{
					if(this._stateList[i].onExit)
						this._stateList[i].onExit();
						
					break;
				}
			}
		}
		
		// change our internal state
		this._state = index;
		
		// run onEnter function
		i = 0;
		for(i; i < this._stateList.length; i++)
		{
			if(this._stateList[i].name === this._state)
			{
				if(this._stateList[i].onEnter)
					this._stateList[i].onEnter();
				
				break;
			}
		}
		
		console.log("Changed state of entity " + this + " to " + this._state);
	},

	getState : function()
	{
		return this._state;
	},

	init : function()
	{
		// getter and setter for _state, so that networked versions of this entity can directly change _state and still have the changeState handler run
		this.__defineGetter__("state", function() { return this._state; });
		this.__defineSetter__("state", function(input) { this.changeState(input); });
	},

	update : function(dt, ctx)
	{		
		this._super(dt, ctx);
	
		// run animation stuff
		this.anim._frameCounter += dt;
		if(this.anim._frameCounter > this.anim.framerate)
		{
			this.anim._frameCounter = 0;
			this.anim._frameIndex++;
			
			if(this.anim._frameIndex > this.anim.animationSets[this.anim.animation].length - 1)
			{
				this.anim._frameIndex = 0;
			}
		}
		
		var img = $("#" + this.sprite)[0],
			h = img.height,
			offset = this.anim.animationSets[this.anim.animation][this.anim._frameIndex] * h;
		
		if(ctx)
		{
			ctx.drawImage(img, offset,0, h,h, this.position.x, this.position.y, h,h);
		}
		
	}

});