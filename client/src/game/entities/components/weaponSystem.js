// contains all the functionality for a data-driven weapons system module, which can be attached to an entity, or used on its own
var app = app || {};
app.components = app.components || {};

app.components.weaponSystem = function( owner )
{
	this.owner = owner;

	this.params = {
		type : "linear",
		ammo : 5,
		delay : 500,
		damage : 3,
		velocity : 1000,
	}

	this.timer = 0;

	console.log("Initialized weapon system of type: " + this.params.type + " owned by ID " + this.owner.id);
};

app.components.weaponSystem.prototype.update = function( dt )
{
	this.timer += dt;
}

app.components.weaponSystem.prototype.shoot = function( origin, target, parameters )
{
	if( (this.timer * 1000) > this.params.delay)
	{
		this.timer = 0;

			app.controllers.ballistics.registerShot(
				{
					velocity : this.params.velocity,
					damage : this.params.damage,
					sprite : "bullet",
					faction : this.owner.faction
				},
				origin,
				target,
				this.params.type
		);
	}

}

app.components.weaponSystem.factory = function( params, owner )
{
	var ws = new app.components.weaponSystem( owner );

	// apply specified parameters to our new weapon system, overriding any defaults
	for(p in params)
	{
		ws.params[p] = params[p];
	}

	return ws;
}