// Handles input for the player unit

var app = app || {};
app.components = app.components || {};

app.components.inputHandler = function( owner )
{
	this.owner = owner;

	this.controls = {
		mouse : false,
		mousePos : {x:0, y:0}
	}

	console.log("Initialized input handler owned by ID " + this.owner.id);
}

app.components.inputHandler.prototype.mouseDown = function()
{
	this.controls.mouse = true;
}

app.components.inputHandler.prototype.mouseUp = function()
{
	this.controls.mouse = false;
}

app.components.inputHandler.prototype.mousePos = function( pos )
{
	this.controls.mousePos = pos;
}

app.components.inputHandler.prototype.update = function( dt )
{
	if( this.controls.mouse )
	{
		this.owner.components.weapons.shoot(this.owner.pos, this.controls.mousePos);
	}
}

app.components.inputHandler.factory = function( params, owner )
{
	var ip = new app.components.inputHandler( owner );

	// apply specified parameters to our new weapon system, overriding any defaults
	for(p in params)
	{
		ip.params[p] = params[p];
	}

	return ip;
}