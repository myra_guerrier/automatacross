// energy shields, which deflect incoming damage to the mech

var app = app || {};
app.components = app.components || {};

app.components.shieldGenerator = function( owner )
{
	this.owner = owner;
	this.alpha = 0.25;
	this.currCapacity = 50;
	this.buffer = buffer = $("<canvas/>")[0];
	this.buffer.width = 96;
	this.buffer.height = 96;
	this.bx = buffer.getContext("2d");
	this.rechargeCounter = 0;

	this.params = {
		sprite : $("#sShield")[0],
		flareAlpha : 1,
		steadyAlpha : 0.125,
		steadyTime : 0.7,
		capacity : 50,
		rechargeRate : 5,
		rechargeDelay : 3
	}
}

app.components.shieldGenerator.prototype.absorb = function( d )
{
	this.alpha = this.params.flareAlpha;

	this.currCapacity -= d;

	this.currCapacity = Math.floor(this.currCapacity);

	if(this.currCapacity < 0)
	{
		this.rechargeCounter = 0;
		d = -1 * this.currCapacity
		this.currCapacity = 0;
		return d;
	}

	return 0;
}

app.components.shieldGenerator.prototype.fixedUpdate = function( dt, ctx )
{
	var r = (this.currCapacity / this.params.capacity);
	
	this.bx.save();

	this.bx.fillStyle = "rgb(" +
		Math.floor( 255 * (1 - r) ) +
		"," + Math.floor(128 * r) + "," +
		Math.floor(255 * r) + ")";

	this.bx.fillRect(0,0,96,96);
	this.bx.globalCompositeOperation = "destination-atop";
	this.bx.drawImage(this.params.sprite, 0,0);

	this.bx.restore();
}

app.components.shieldGenerator.prototype.update = function( dt, ctx )
{
	var r = (this.currCapacity / this.params.capacity);

	if(this.currCapacity < this.params.capacity && this.currCapacity > 0)
	{
		this.currCapacity += (dt * this.params.rechargeRate);
	}
	else if(this.currCapacity <= 0)
	{
		this.rechargeCounter += dt;

		if(this.rechargeCounter >= this.params.rechargeDelay)
		{
			this.alpha = this.params.flareAlpha;
			this.currCapacity += (dt * this.params.rechargeRate);
			this.rechargeCounter = 0;
		}
	}

	this.alpha = this.alpha - (dt / this.params.steadyTime);
	if( this.alpha < this.params.steadyAlpha )
		this.alpha = this.params.steadyAlpha;

	if(this.currCapacity > 0)
	{
		ctx.save();

		ctx.globalCompositeOperation = "lighter";
		ctx.globalAlpha = this.alpha;

		ctx.drawImage( this.buffer,
			this.owner.pos.x - app.game.cam.x - 48,
			this.owner.pos.y - app.game.cam.y - 48 );

		ctx.restore();
	}
}

app.components.shieldGenerator.factory = function( params, owner )
{
	var sg = new app.components.shieldGenerator( owner );

	for( p in params )
	{
		sg.params[p] = params[p];
	}

	return sg;
}