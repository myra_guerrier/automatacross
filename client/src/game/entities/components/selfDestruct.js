// self-destruct system.  Does exactly what you'd expect.

var app = app || {};
app.components = app.components || {};

app.components.selfDestruct = function( owner )
{
	this.owner = owner;

	this.params = {
		size : 250,
		damage : 20
	};

	console.log("Initialized self-destruct system owned by ID " + this.owner.id);
}

app.components.selfDestruct.prototype.activate = function()
{
	var i = 0,
		l = app.game.simList,
		lim = l.length;

	for( i; i < lim; i++ )
	{
		if( l[i].id )
		{
				if( app.vector.compare( l[i].pos, this.owner.pos, this.params.size ) )
				{
					l[i].damage( this.params.damage );
				}
		}
	}

	this.owner.damage( 999999 );
}

app.components.selfDestruct.factory = function( params, owner )
{
	var sd = new app.components.selfDestruct( owner );

	// apply specified parameters to our new weapon system, overriding any defaults
	for(p in params)
	{
		sd.params[p] = params[p];
	}

	return sd;
}