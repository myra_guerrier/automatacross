// basic AI behaviors -- target pursuit, combat routines, mostly hard-coded, to be used by minimal-complexity enemies and for testing

var app = app || {};
app.components = app.components || {};

app.components.basicAIPackage = function( owner )
{
	this.owner = owner;

	this.params = {
		type : "fixedDistance",
		range : 250
	};

	console.log("Initialized AI package owned by ID " + this.owner.id);
}


app.components.basicAIPackage.prototype.fixedUpdate = function( dt, ctx )
{
	var sensors = this.owner.components.sensors;

	if( !sensors )
	{
		console.warn("AI unit owned by " + this.owner.id + " has no sensors!  Disabling AI behaviors!");
		this.params.type = null;
	}

	switch( this.params.type )
	{
		// ----------------------------------------------------------------------------------------------------------------------------------
		// charge at the player blindly, shooting if we have a weapon
		case "charge":

			if( sensors.targets[0] )
			{
				this.owner.target = sensors.targets[0].pos;

				if( this.owner.components.weapons )
				{
					this.owner.components.weapons.shoot( this.owner.pos, sensors.targets[0].pos );
				}

				if( this.owner.components.selfDestruct )
				{
					if( app.vector.compare( this.owner.pos, sensors.targets[0].pos, 50 ) )
					{
						this.owner.components.selfDestruct.activate();
					}
				}
			}

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		// stay still and shoot if the player comes into range
		case "turret":


			if( sensors.targets[0] )
			{
				this.owner.heading = sensors.targets[0].pos;

				if( this.owner.components.weapons )
				{
					this.owner.components.weapons.shoot( this.owner.pos, sensors.targets[0].pos );
				}
			}

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		// stay within weapons range of the player, but don't close distance
		case "fixedDistance":

			if( sensors.targets[0] )
			{
				this.owner.heading = sensors.targets[0].pos;
				
				if( ! app.vector.compare(this.owner.pos, sensors.targets[0].pos, this.params.range) )
				{
					this.owner.target = sensors.targets[0].pos;
				}

				if( this.owner.components.weapons )
				{
					this.owner.components.weapons.shoot( this.owner.pos, sensors.targets[0].pos );
				}
			}

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		// Launch timed barrages of shots with any missile launchers, setting to normal behavior otherwise
		case "volley":

			if( sensors.targets[0] )
			{
				this.owner.heading = sensors.targets[0].pos;

				this.volleyCounter += dt;

				// fire a barrage of missiles every few seconds
				if( this.volleyCounter > this.params.volleyDelay )
				{
					this.owner.components.missiles.shoot( this.owner.pos, sensors.targets[0].pos );

					if( this.volleyCounter > this.params.volleyDelay + this.params.volleyLength )
					{
						this.volleyCounter = 0;
					}
				}

				// use conventional weapons systems as well
				else if( this.owner.components.weapons )
				{
					this.owner.components.weapons.shoot( this.owner.pos, sensors.targets[0].pos );
				}

				// move toward the player, keeping them in range
				if( ! app.vector.compare(this.owner.pos, sensors.targets[0].pos, this.params.range) )
				{
					this.owner.target = sensors.targets[0].pos;
				}
			}

			break;

		// ----------------------------------------------------------------------------------------------------------------------------------
		default:
			break;
	}
}

app.components.basicAIPackage.factory = function( params, owner )
{
	var ai = new app.components.basicAIPackage( owner );

	// apply specified parameters to our new weapon system, overriding any defaults
	for(p in params)
	{
		console.log(params[p]);
		ai.params[p] = params[p];
	}

	if(ai.params.type === "volley")
	{
		ai.volleyCounter = 0;
	}

	return ai;
}