// Mech sensor package, used for presenting sensory data to other mech systems based on input parameters

var app = app || {};
app.components = app.components || {};

app.components.sensorPackage = function( owner )
{
	this.owner = owner;

	this.params = {
		type : "radial",
		range : 450,
		search : "player"
	};

	console.log("Initialized sensors owned by ID " + this.owner.id);
}


app.components.sensorPackage.prototype.fixedUpdate = function( dt )
{
	// generate a list of targets in range, with basic radial range testing

	var pos = this.owner.pos,
		l = app.game.simList
		i = 0,
		lim = l.length,
		rList = [];

	for(i; i < lim; i++)
	{
		if( l[i].type && l[i].type === this.params.search )
		{
			if( app.vector.compare( pos, l[i].pos, this.params.range ) )
			{
				rList.push(l[i]);
			}
		}
	}

	this.targets = rList;
}

app.components.sensorPackage.factory = function( params, owner )
{
	var sp = new app.components.sensorPackage( owner );

	// apply specified parameters to our new weapon system, overriding any defaults
	for(p in params)
	{
		sp.params[p] = params[p];
	}

	return sp;
}