// a small sprite which is displayed when the user taps or clicks, to make the action visible

var app = app || {};

app.game.ping = {

	pos : {x : 0, y : 0},
	lifetime : 0,
	sprite : $("#sPing")[0],
	params : {
		time : 0.5,
		maxScale : 1.6
	},

	at : function( pos )
	{
		this.pos = pos;
		this.lifetime = 0;
	},

	update : function( dt, ctx )
	{
		this.lifetime += dt;

		if(this.lifetime < this.params.time)
		{
			var t = this.lifetime/this.params.time;

			ctx.save();
			ctx.globalAlpha = 1 - t;
			ctx.translate( this.pos.x - app.game.cam.x, this.pos.y - app.game.cam.y );
			ctx.scale( t * this.params.maxScale, t * this.params.maxScale );
			ctx.drawImage(this.sprite, -16, -16);
			ctx.restore();
		}
	}
}