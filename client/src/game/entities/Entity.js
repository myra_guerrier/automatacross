// Entity -- the base class from which all simulation entities derive

var app = app || {};

// create an entity definition using the BaseClass function
app.game.entity = Class.extend( {

	// our location (in squares)
	pos : {
		x : 0,
		y : 0,
		z : 0
	},
	
	// updates the entity, (almost) always called once per draw cycle, with parameters deltaTime and ctx, the time since the last update, and the context onto which we should draw
	update : function(dt, ctx)
	{
		console.log("Warning:  Called the update function of the base Entity class, this is not normally possible!");
	}

});