var app = app || {};
app.player = app.player || {};
app.components = app.components || {};

app.game.mech = app.game.characterEntity.extend({

	init : function()
	{
		this.id = app.game.state.getUniqueID();	// unique ID code; set on initialization
		this.status = 0;			// bitfield for status effects
		this.chasis = "default";	// which type of mech we are
		this.type = "enemy";		// are we a player-controlled unit; a neutral NPC; or a foe
		this.gear = {};			// array to hold all current gear and weapons

		this.components = {};

		this.sprite = $("#sLightMech")[0];
		this.turretSprite = $("#sLightMechTurret")[0];

		this.maxHP = 100;
		this.hp = 100;
		this.maxEnergy = 0;
		this.energy = 0;
		this.maxArmor = 0;
		this.armor = 0;
		this.maxSpeed = 175;
		this.speed = 175;
		this.acceleration = 280;
		this.currSpeed = 0;

		this.controlMode = 0;		// 0 - normal mouse input, 1 - accelerometer control
		this.vector = {x:0, y:0};	// current movement vector
		this.target = {x:0, y:0};	// a placeholder toward which a mech may be sent by several movement modes (click to move; knockback; jumpjets)
		this.heading = {x:0, y:0};	// vector to face our weapons along
		this.bearing = {x:0, y:0};

		this.s = {			// bitfield values
			DEAD : 				1<<0,	// dead; to be removed from the simulation
			DISABLED : 			1<<1,	// accept no inputs; take no damage; use no energy; run no AI stuff
			MOVE_DISABLED : 	1<<2,	// accept no movement inputs
			STUNNED : 			1<<3,	// accept no combat or movement inputs; but still take damage
			SUPPRESSED :		1<<4,	// accept no combat inputs; but allow movement
			SLOWED : 			1<<5	// move and act slower
		};


		this.pos = {x : 0, y : 0};

		console.log(" > Created Mech with ID: " + this.id);

		this.components['weapons'] = app.components.weaponSystem.factory({ammo : 10}, this);
		this.components['sensors'] = app.components.sensorPackage.factory({}, this);

		// create several states for our internal FSM

		// IDLE -- nothing affecting us, act and respond to input as normal
		this.addState(
			"idle"
			);

		// THROWN -- used for jump jets, knockback attacks, and charges
		this.addState(
			"thrown",
			function(){
				this.addStatus(this.s.MOVE_DISABLED);
				this.vector = app.vector.vectorTo(this.pos, this.target)
			},
			function(){
				if(app.vector.compare(this.pos, this.target)){
					this.setState("idle");
				}
			},
			function(){
				this.removeStatus(this.s.MOVE_DISABLED);
			});
	},

	// fixed update, called every 1/10th of a second, used for AI and state calculations
	// this accepts delta time as an argument, because frame timing in JS can be somewhat erratic
	fixedUpdate : function(dt, ctx)
	{
		if( this.getStatus(this.s.DEAD) )
		{
			return;
		}

		// check if any of our components have update functions, and update them if so
		for(c in this.components)
		{
			if(this.components[c].fixedUpdate)
			{
				this.components[c].fixedUpdate( dt, ctx );
			}
		}
	},

	// regular update, called once every frame, used for movement and similar
	update : function(dt, ctx)
	{
		var vTheta = 0,
			hTheta = 0;

		if( this.getStatus(this.s.DEAD) )
		{
			ctx.drawImage(this.sprite, this.pos.x - 32 - app.game.cam.x, this.pos.y - 32 - app.game.cam.y);
			return;
		}

		switch( this.controlMode )
		{
			// ------------------------------------------------------------------------------------------------------------------------
			// mouse movement
			case 0:
				if(app.vector.compare(this.pos, this.target) !== true)
				{
					this.vector = app.vector.vectorTo(this.pos, this.target);

					if(this.currSpeed < this.speed && app.vector.distanceSquared(this.pos, this.target) > 50 * 50)
					{
						this.currSpeed = this.currSpeed + (this.acceleration * dt);
					}
					else if(this.currSpeed > 0)
					{
						this.currSpeed = this.currSpeed - (this.acceleration * dt);
					}

					// run positional updates
					this.pos = app.vector.add(
							app.vector.scalarMul(
								this.vector,
								this.currSpeed * dt),
							this.pos);
				}
				else
				{
					this.currSpeed = 0;
				}

				break;

			// ------------------------------------------------------------------------------------------------------------------------
			// accelerometer control
			case 1:

				var newPos = app.vector.add(
						app.vector.scalarMul( 
							this.bearing,
							( dt * this.maxSpeed )
						),
						this.pos
					);

				// check collision, move us if we didn't hit anything
				if( ! app.game.map.checkCollision( newPos ) )
				{
					this.pos = newPos;
				}

				break;

			// ------------------------------------------------------------------------------------------------------------------------
			default:
				console.error("Unknown control mode: " + this.controlMode + "!");
				break;
		}

		

		vTheta = (this.pos.x - this.target.x < 0) ? 
			Math.atan( ((this.pos.y - this.target.y) / (this.pos.x - this.target.x)) ) :
			Math.atan( ((this.pos.y - this.target.y) / (this.pos.x - this.target.x)) ) + Math.PI;

		hTheta = (this.pos.x - this.heading.x < 0) ? 
			Math.atan( ((this.pos.y - this.heading.y) / (this.pos.x - this.heading.x)) ) :
			Math.atan( ((this.pos.y - this.heading.y) / (this.pos.x - this.heading.x)) ) + Math.PI;

		//ctx.fillText(("Pos: " + this.pos.x + ", " + this.pos.y), 50, 50);
		//ctx.fillText(("Vec: " + this.vector.x + ", " + this.vector.y), 50, 70);
		//ctx.fillText(("Spd: " + this.speed), 50, 90);
		//ctx.fillText(("vTheta: " + vTheta), 50, 110);
		//ctx.fillText(("hTheta: " + hTheta), 50, 130);

		// draw the mech
		ctx.save();
		ctx.translate(this.pos.x - app.game.cam.x, this.pos.y - app.game.cam.y);
		ctx.rotate(vTheta + Math.PI/2);
		ctx.drawImage(this.sprite, -32, -32);
		ctx.restore();

		// draw the turret, with offset rotation
		ctx.save();
		ctx.translate(this.pos.x - app.game.cam.x, this.pos.y - app.game.cam.y);
		ctx.rotate(hTheta + Math.PI/2);
		ctx.drawImage(this.turretSprite, -32, -32);
		ctx.restore();

		if(this.type === "player")
		{
			/*ctx.save();
			ctx.fillStyle = "rgba(255,255,255,1)";
			ctx.font = "bold 16px Arial";
			ctx.fillText(("HP: " + this.hp + "/" + this.maxHP), 40, 40)
			ctx.restore();*/

			$("#hudHP").css( "width", ( (this.hp / this.maxHP) * 100 ) + "%" ).text("HP " + this.hp/* + "/" + this.maxHP*/);
			//$("$hudShield").text( (this. + " / " + this.maxHP) )

			$("#hudShield").css( "width", ( (this.components.shields.currCapacity / this.components.shields.params.capacity) * 100 ) + "%" )
				.text("Shields " + Math.floor(this.components.shields.currCapacity)/* + "/" + this.components.shields.params.capacity*/);
		}

		// check if any of our components have update functions, and update them if so
		for(c in this.components)
		{
			if(this.components[c].update)
			{
				this.components[c].update( dt, ctx );
			}
		}
	},

	// moves us instantly to the requested location
	teleport : function(target)
	{
		this.pos = target;
	},

	moveTo : function(target)
	{
		this.target = app.vector.add(
			target,
			this.pos
			);
	},

	setVector : function(vec)
	{
		this.vector = vec;
	},

	shift : function( amount )
	{
		this.target = app.vector.add(
			this.target,
			amount
			);
	},

	// check if we have the specified status
	getStatus : function(status)
	{
		return (this.status & status > 0);
	},

	// applies a status to the status bitfield
	// accepts either a status (as an integer), or an array of statuses
	applyStatus : function(status)
	{
		if(status.length)
		{
			for(var i = 0, lim = status.length; i < lim; i++)
			{
				this.status |= status[i];
			}
		}
		else
		{
			this.status |= status;
		}
	},

	// removes a status from the bitfield
	// accepts either an integer status, or a list of integer statuses
	removeStatus : function(status)
	{
		if(status.length)
		{
			for(var i = 0, lim = status.length; i < lim; i++)
			{
				this.status &= (~ status[i]);
			}
		}
		else
		{
			this.status &= (~ status);
		}
	},

	damage : function(d)
	{
		if(this.components['shields'])
		{
			d = this.components['shields'].absorb( d );
		}

		if(this.hp <= 0)
		{
			this.applyStatus(this.s.DEAD);
			this.delete = true;

			app.controllers.particleSystem.newEmitter(
			{
				count : 3,
				spawnRate : 0,
				lifetime : 0.25,
				systemLifetime : 1,
				initVelocity : 25,
				velocityJitter : 15,
				acceleration : -50,
				initPos : this.pos,
				spawnRadius : 1,
				color : "rgba(255,64,0,1)",
				randomSpin : true,
				angularVelocity : 2,
				angularAcceleration : -1,
				initScale : 1.5,
				endScale : 4
			},
			"sFire");

			app.controllers.particleSystem.newEmitter(
			{
				count : 15,
				spawnRate : 0,
				lifetime : 0.6,
				systemLifetime : 1,
				initVelocity : 400,
				velocityJitter : 300,
				acceleration : -1000,
				initPos : this.pos,
				spawnRadius : 1,
				color : "rgba(255,64,0,1)",
				initScale : 1.5,
				endScale : 0.25
			},
			"sGlow");

			app.controllers.particleSystem.newEmitter(
			{
				count : 35,
				spawnRate : 0,
				lifetime : 1.5,
				systemLifetime : 0.01,
				initVelocity : 140,
				velocityJitter : 40,
				acceleration : -160,
				initPos : this.pos,
				spawnRadius : 10,
				color : "rgba(255,64,0,1)",
				randomSpin : true,
				angularVelocity : 2,
				angularAcceleration : -1,
				initScale : 1,
				endScale : 2
			},
			"sBoom");
		}
		else
		{
			//console.log("Mech " + this.id + " took " + d + " points of damage, HP " + this.hp);
			this.hp -= d;
		}
	},

	query : function( q )
	{
		if( q )
		{
			console.log(( "%c Displaying status of mech: " + this.id + ":" ), "color:gray" );
			console.log(( "%c  > " + JSON.stringify(this.components[q]) ), "color:gray" );
		}
		else
		{
			console.log(( "%c Displaying status of mech: " + this.id + ":" ), "color:gray");

			for(c in this.components)
			{
				// console.log(( "%c  > " + JSON.stringify(this.components[c]) ), "color:gray");
				console.log(this.components[c]);
			}
		}
	}

});