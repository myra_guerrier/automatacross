// Use Modernizr to load all relevent scripts

var app = app || {};

yepnope([
{
	test : window.requestAnimationFrame,
	nope : "src/polyfils/requestAnimationFrame.js"
},
{
	
	load : [
		// libraries
		"src/lib/jquery-1.9.1.js",
		"src/lib/jquery.imagesloaded.js",

		// core functionality
		"src/app.js",

		// utility
		"src/util/Vector.js",
		"src/util/Time.js",
		"src/util/CanvasGraphics.js",

		// screens
		"src/screens/acGame.js",
		"src/screens/acMissionConfirm.js",

		// controllers
		// systems for handling the behaviors of groups of objects in the simulation, loaded before core game code since many game systems depend on controllers existing already
		"src/game/controllers/ballistics.js",

		// components
		// several types of game entities depend on these, so we load them before the core game code
		"src/game/entities/components/weaponSystem.js",
		"src/game/entities/components/sensorPackage.js",
		"src/game/entities/components/basicAIPackage.js",
		"src/game/entities/components/inputHandler.js",
		"src/game/entities/components/shieldGenerator.js",
		"src/game/entities/components/selfDestruct.js",

		// game code
		"src/game/game.js",
		"src/game/gameController.js",
		"src/game/gameState.js",
		"src/game/gameParams.js",
		"src/game/virtualCamera.js",
		"src/game/gameMap.js",
		"src/game/entities/BaseClass.js",
		"src/game/entities/Entity.js",
		"src/game/entities/CharacterEntity.js",
		"src/game/entities/Mech.js",
		"src/game/entities/MechFactory.js",
		"src/game/entities/Ping.js",

		// particles
		"src/game/controllers/Particle.js",
		"src/game/controllers/ParticleEmitter.js",
		"src/game/controllers/ParticleSystem.js"
	],

	complete : function() { 

		// grab a config file, setting up some application-wide parameters, including base url for AJAX calls

		var environment = "";

		switch(window.location.origin)
		{
			case "http://automata-cross.herokuapp.com":
				environment = "heroku";
				break;

			case "http://localhost:3000":
				environment = "local";
				break;

			default:
				environment = "default";
				break;
		}

		$.ajax({
			url : "config/" + environment + ".json"
		}).done(function(result)
		{
			console.log("Got app config values: ");
			console.log(result);
			app.config = result;

			// initialize the application!
			app.init();
		});
	}
	
}]);