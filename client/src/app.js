var app = app || {};

app.screens = {};

app.init = function()
{
	console.log("Initialized the application...");

	// set up main menu click detection
	$("#acMenu ul li button").click(function(event)
	{
		try
		{
		app.activateScreen(event.target.name);
		}
		catch(ex)
		{
			alert(ex);
		}

		return false;
	});

	// set up some return buttons so that the user can get back to the main menu
	$(".acBackButton").click(function(event)
	{
		app.activateScreen("acMenu");
	});

	// activate the main menu
	app.activateScreen("acMenu");

	// get an objective from the server at app startup
	$.ajax({
		url : app.config.root_url + "/new-mission"
	}).done(function(result)
	{
		app.screens.acMissionConfirm.updateDisplay(JSON.parse(result));
	});
}

app.activateScreen = function(id)
{
	var oldScreen = $("#acBody .screen.active"),
		newScreen = $("#" + id);

	console.log(" > Switching to screen " + id);

	// if the old screen exists, run its exit function
	if(oldScreen)
	{
		oldScreen.removeClass("active");

		if(oldScreen[0] && app.screens[oldScreen[0].id])
		{
			app.screens[oldScreen[0].id].onExit();
		}
	}

	// run the onEnter function for the new screen, and make it visible
	newScreen.addClass("active");

	if(app.screens[newScreen[0].id] && app.screens[newScreen[0].id].onEnter)
	{
		app.screens[newScreen[0].id].onEnter();
	}
}