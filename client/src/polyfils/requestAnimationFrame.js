(function() {
	var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

	if(requestAnimationFrame)
	{
		window.requestAnimationFrame = requestAnimationFrame;
	}
	else
	{
		window.requestAnimationFrame = function(f)
		{
			setTimeout(f, 33);
		}
	}
})();